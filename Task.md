

A User authenticated Backend.

This projects contains a User Authenticated backend.

Prerequisites:

 - Install Python 3.6
 - Install a local MongoDB.
 - Create a users collection in db local.
 - Install the project dependencies in requirements.txt.
 - Fork the project at <>
 - Share the project with user: mpousa


# Task 1. Find the bugs, make the tests pass

The script `test-script.sh` will execute pylint,pycodestyle,pydocstyle to check for the style
of the code, currently is not passing this first step. Make it pass.
If a linter error is unavoidable disable it, use your better judgement when to do that.


Afterwards execute the tests in the folder tests/.
You will see that some of the tests are failling.
Your task is to fix the issue and make them pass.

# Task 2. Add city to User Profile

The task is to add a field city to every User.
From now on every user should have:
 - email
 - password
 - city


The city of a user can only be one of this:
  - Berlin
  - New York
  - Helsinki
  - Rome
  - Budapest
  - New Delhi



# Task 3. Design and Document the Architecture

Design the infraestructure that will be needed to support the system.
Create a diagram in https://cloudcraft.co/ and added as documentation
to the repository.
Restrict yourself to use AWS and Docker.
Assume peak traffics to the API of 50 RPM and a maximum of 1000 Users.
Maintain very high availability.


