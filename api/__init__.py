# encoding: utf-8
"""Example."""
import logging


def main():
    """Main."""
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    logging.debug("Main.")


if __name__ == '__main__':
    main()
