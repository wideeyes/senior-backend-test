# encoding: utf-8
"""Data Mappers."""
import logging
import motor.motor_asyncio


class UserMapper(object):
    """User Mapper."""
    def __init__(self,
                 db_client=None):
        if db_client is None:
            db_client = motor.motor_asyncio.AsyncIOMotorClient()
        self.client = db_client.local.users

    def to_document(self):
        document = {"email": self.email,
                    "password": self.password}
        return document

    async def find(self):
        """Find the user in the db."""
        document = {'email': self.email}
        result = await self.client.find_one(document)
        logging.debug(result)
        if result is None:
            logging.debug("could not find user %s in db", self.email)
            return False
        self.password = result['password']
        self.db_id = result['_id']
        return self

    async def insert(self):
        """Insert into the db."""
        document=self.to_document()
        result = await self.client.insert_one(document)
        self.db_id = result.inserted_id
        return result.inserted_id

    async def update(self):
        """Update the db."""
        document = self.to_document()
        result = await self.client.replace_one({'_id': self.db_id}, document)

    async def delete(self):
        """Delete the object in the db."""
        await self.client.delete_many({'_id': self.db_id})


def main():
    """Main."""
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    logging.debug("Main.")


if __name__ == '__main__':
    main()